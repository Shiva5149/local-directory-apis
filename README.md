# Local Directory API Service

This is a Django Rest Framework API project for [Local Directory]. It provides the backend API endpoints for [Local Directory].

## Prerequisites
Before you begin, make sure you have the following installed:

`Python` (version 3.6 or higher)
`pip` (Python package installer)

## Setup

1. Clone the repository:

`git clone https://gitlab.com/Shiva5149/local-directory-apis.git`
`cd Local Directory APIs`

2. Create and activate a virtual environment (optional but recommended):

`python3 -m venv env`

# for Linux/macOS
`source env/bin/activate`  

# for Windows
`env\Scripts\activate` `


3. Install the required dependencies:

`pip install -r requirements.txt`

## Usage

To start the development server, run the following command:

`python manage.py runserver`


## API Endpoints

    "locations": "http://127.0.0.1:8000/local_directories_api/locations/",
    "users": "http://127.0.0.1:8000/local_directories_api/users/",
    "categories": "http://127.0.0.1:8000/local_directories_api/categories/",
    "services": "http://127.0.0.1:8000/local_directories_api/services/",
    "feedback": "http://127.0.0.1:8000/local_directories_api/feedback/",
    "payment": "http://127.0.0.1:8000/local_directories_api/payment/"

## Swagger UI

![Project Logo](images/local_directory_swagger_ui.png)

## Contact

```
**Developed by**: Shiva
**Contact**: 8309025511
**mail**: shivashankarpallapu@gmail.com
**Team**: **Local Directory**__
**Authority**: **Local Directory API Service**__
```

