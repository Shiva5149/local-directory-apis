from django.contrib.auth.models import AbstractUser, Group, Permission
from django.db import models


class Locations(models.Model):
    address = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    country = models.CharField(max_length=200, blank=True, null=True)
    latitude = models.CharField(max_length=100, blank=True, null=True)
    longitude = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.address


class CustomUser(AbstractUser):
    USER_TYPES = [
        ('regular', 'Regular User'),
        ('service_provider', 'Service Provider'),
    ]
    name = models.CharField(max_length=200, blank=True, null=True)
    mobile_number = models.CharField(max_length=15, blank=True, null=True)
    user_type = models.CharField(
        max_length=100, choices=USER_TYPES, default='regular')
    location = models.ForeignKey(
        'Locations', on_delete=models.CASCADE, blank=True, null=True, related_name='user_locations')
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Provide unique related_name for groups and user_permissions fields
    groups = models.ManyToManyField(
        Group, blank=True, related_name='customuser_set', related_query_name='customuser'
    )
    user_permissions = models.ManyToManyField(
        Permission, blank=True, related_name='customuser_set', related_query_name='customuser'
    )

    def __str__(self):
        return self.name


class Categories(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    category_image = models.ImageField(
        upload_to='categories/', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Services(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    amount = models.CharField(max_length=200, blank=True, null=True)
    provider = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    service_image = models.ImageField(
        upload_to='services/', blank=True, null=True)
    category = models.ForeignKey(
        Categories, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Feedback(models.Model):
    rating = models.CharField(max_length=10, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    user = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    service = models.ForeignKey(
        Services, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.service.name


class Payment(models.Model):
    amount = models.IntegerField(null=True, blank=True)
    payment_date = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, blank=True, null=True)
    service = models.ForeignKey(
        Services, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.amount)
