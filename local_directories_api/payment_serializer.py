from rest_framework import serializers
from .models import Services, CustomUser, Payment
from .serializers import UserSerializer
from .services_serializer import ServicesSerializer


class PaymentSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    service = ServicesSerializer()

    class Meta:
        model = Payment
        fields = ['id', 'amount', 'payment_date',
                  'user', 'service', 'is_active', 'created_at', 'updated_at']

    def to_internal_value(self, data):
        user_data = data.pop('user', {})
        user_id = user_data.get('id')
        service_data = data.pop('service', {})
        service_id = service_data.get('id')
        validated_data = super().to_internal_value(data)

        if user_id:
            try:
                user = CustomUser.objects.get(id=user_id)
                validated_data['user'] = user
            except CustomUser.DoesNotExist:
                raise serializers.ValidationError("Invalid user ID")
        if service_id:
            try:
                service = Services.objects.get(id=service_id)
                validated_data['service'] = service
            except Services.DoesNotExist:
                raise serializers.ValidationError("Invalid service ID")

        return validated_data

    def create(self, validated_data):
        user_data = validated_data.pop('user', None)
        service_data = validated_data.pop('service', None)
        feedback = Payment.objects.create(**validated_data)

        if user_data:
            user_serializer = self.fields['user']
            user = user_serializer.create(user_data)
            feedback.user = user
            feedback.save()

        if service_data:
            service_serializer = self.fields['service']
            service = service_serializer.create(service_data)
            feedback.service = service
            feedback.save()
        return feedback

    def update(self, instance, validated_data):
        user_data = validated_data.pop('user', None)
        service_data = validated_data.pop('service', None)
        feedback = super().update(instance, validated_data)

        if user_data:
            user_serializer = self.fields['user']
            user = user_serializer.update(
                instance.user, user_data)
            feedback.user = user
            feedback.save()
        if service_data:
            service_serializer = self.fields['service']
            service = service_serializer.update(
                instance.service, service_data)
            feedback.service = service
            feedback.save()

        return feedback
