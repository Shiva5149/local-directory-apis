from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from .models import Categories
from .categories_serializer import CategoriesSerializer
from .pagination import Pagination


class CategoryViewSet(ModelViewSet):
    queryset = Categories.objects.all()
    serializer_class = CategoriesSerializer
    pagination_class = Pagination
    filter_backends = [SearchFilter]
    search_fields = ['name', 'description']
