from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from .models import Services
from .services_serializer import ServicesSerializer
from .pagination import Pagination


class ServiceViewSet(ModelViewSet):
    queryset = Services.objects.all()
    serializer_class = ServicesSerializer
    pagination_class = Pagination
    filter_backends = [SearchFilter]
    search_fields = ['name', 'description', 'amount',
                     'provider', 'service_image', 'category', 'is_active', 'created_at', 'updated_at']
