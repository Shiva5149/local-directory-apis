from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from .models import Feedback
from .feedback_serializer import FeedbackSerializer
from .pagination import Pagination


class FeedbackViewSet(ModelViewSet):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    pagination_class = Pagination
    filter_backends = [SearchFilter]
    search_fields = ['rating', 'comment']
