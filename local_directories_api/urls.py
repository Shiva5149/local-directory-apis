from django.urls import path, include
from rest_framework.routers import DefaultRouter
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

from .views import LocationViewSet, UserViewSet
from .category_views import CategoryViewSet
from .service_views import ServiceViewSet
from .feedback_views import FeedbackViewSet
from .payment_views import PaymentViewSet


router = DefaultRouter()
router.register(r'locations', LocationViewSet)
router.register(r'users', UserViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'services', ServiceViewSet)
router.register(r'feedback', FeedbackViewSet)
router.register(r'payment', PaymentViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title="Local Directory API Documentation",
        default_version='v1',
        description="Local Directory API documentation powered by Local Directory Team",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('', schema_view.with_ui('swagger',
         cache_timeout=0), name='swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='redoc'),
    path('local_directories_api/', include(router.urls)),
]
