from django.apps import AppConfig


class LocalDirectoriesApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'local_directories_api'
