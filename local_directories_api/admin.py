from django.contrib import admin
from .models import Locations, CustomUser, Categories, Services, Feedback, Payment
# Register your models here.

admin.site.register(Locations)
admin.site.register(CustomUser)
admin.site.register(Categories)
admin.site.register(Services)
admin.site.register(Feedback)
admin.site.register(Payment)
