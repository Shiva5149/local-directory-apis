from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from .models import Locations, CustomUser
from .serializers import LocationsSerializer, UserSerializer
from .pagination import Pagination

class LocationViewSet(ModelViewSet):
    queryset = Locations.objects.all()
    serializer_class = LocationsSerializer
    pagination_class = Pagination
    filter_backends = [SearchFilter]
    search_fields = ['address', 'city', 'state',
                     'country', 'latitude', 'longitude']


class UserViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    pagination_class = Pagination
    filter_backends = [SearchFilter]
    search_fields = ['name', 'location', 'mobile_number',
                     'user_type', 'is_active', 'created_at', 'updated_at']
