from rest_framework import serializers
from .models import Locations, CustomUser


class LocationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Locations
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    location = LocationsSerializer()

    class Meta:
        model = CustomUser
        fields = ['id', 'name', 'location', 'mobile_number',
                  'user_type', 'is_active', 'created_at', 'updated_at']

    def to_internal_value(self, data):
        location_data = data.pop('location', {})
        location_id = location_data.get('id')
        validated_data = super().to_internal_value(data)

        if location_id:
            try:
                location = Locations.objects.get(id=location_id)
                validated_data['location'] = location
            except Locations.DoesNotExist:
                raise serializers.ValidationError("Invalid location ID")

        return validated_data

    def create(self, validated_data):
        location_data = validated_data.pop('location', None)
        user = CustomUser.objects.create(**validated_data)

        if location_data:
            location_serializer = self.fields['location']
            location = location_serializer.create(location_data)
            user.location = location
            user.save()

        return user

    def update(self, instance, validated_data):
        location_data = validated_data.pop('location', None)
        user = super().update(instance, validated_data)

        if location_data:
            location_serializer = self.fields['location']
            location = location_serializer.update(
                instance.location, location_data)
            user.location = location
            user.save()

        return user
