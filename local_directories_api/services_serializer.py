from rest_framework import serializers
from .models import Services, CustomUser, Categories
from .serializers import UserSerializer
from .categories_serializer import CategoriesSerializer


class ServicesSerializer(serializers.ModelSerializer):
    provider = UserSerializer()
    category = CategoriesSerializer()

    class Meta:
        model = Services
        fields = ['id', 'name', 'description', 'amount',
                  'provider', 'service_image', 'category', 'is_active', 'created_at', 'updated_at']

    def to_internal_value(self, data):
        provider_data = data.pop('provider', {})
        provider_id = provider_data.get('id')
        category_data = data.pop('category', {})
        category_id = category_data.get('id')
        validated_data = super().to_internal_value(data)

        if provider_id:
            try:
                provider = CustomUser.objects.get(id=provider_id)
                validated_data['provider'] = provider
            except CustomUser.DoesNotExist:
                raise serializers.ValidationError("Invalid provider ID")
        if category_id:
            try:
                category = Categories.objects.get(id=category_id)
                validated_data['category'] = category
            except Categories.DoesNotExist:
                raise serializers.ValidationError("Invalid category ID")

        return validated_data

    def create(self, validated_data):
        provider_data = validated_data.pop('provider', None)
        category_data = validated_data.pop('category', None)
        service = Services.objects.create(**validated_data)

        if provider_data:
            provider_serializer = self.fields['provider']
            provider = provider_serializer.create(provider_data)
            service.provider = provider
            service.save()

        if category_data:
            category_serializer = self.fields['category']
            category = category_serializer.create(category_data)
            service.category = category
            service.save()
        return service

    def update(self, instance, validated_data):
        provider_data = validated_data.pop('provider', None)
        category_data = validated_data.pop('category', None)
        service = super().update(instance, validated_data)

        if provider_data:
            provider_serializer = self.fields['provider']
            provider = provider_serializer.update(
                instance.provider, provider_data)
            service.provider = provider
            service.save()
        if category_data:
            category_serializer = self.fields['category']
            category = category_serializer.update(
                instance.category, category_data)
            service.category = category
            service.save()

        return service
