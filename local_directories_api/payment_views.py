from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from .models import Payment
from .payment_serializer import PaymentSerializer
from .pagination import Pagination


class PaymentViewSet(ModelViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    pagination_class = Pagination
    filter_backends = [SearchFilter]
    search_fields = ['amount', 'payment_date']
